import { Component } from "react";
import style from "../../../App.module.css";
class FooterSocial extends Component{
    render(){
        return(
            <>
                <div className="footer-social">
                    <div className="container-fluid text-center">
                        <div className="row">
                           <div className="col-sm-3" style={{border: "1px solid hsla(0,0%,100%,.1)" , margin : "auto" , padding : "15px 0"}}>
                                <a style={{color : "white" , textDecoration : "none"}} href="#">
                                    <span>FACEBOOK</span>
                                </a>
                           </div>
                           <div className="col-sm-3" style={{border: "1px solid hsla(0,0%,100%,.1)" , margin : "auto" , padding : "15px 0"}}>
                                <a style={{color : "white" , textDecoration : "none"}} href="#">
                                    <span>INSTAGRAM</span>
                                </a>
                           </div>
                           <div className="col-sm-3" style={{border: "1px solid hsla(0,0%,100%,.1)" , margin : "auto" , padding : "15px 0"}}>
                                <a style={{color : "white" , textDecoration : "none"}} href="#">
                                    <span>TWITTER</span>
                                </a>
                           </div>
                           <div className="col-sm-3" style={{border: "1px solid hsla(0,0%,100%,.1)" , margin : "auto" , padding : "15px 0"}}>
                                <a style={{color : "white" , textDecoration : "none"}} href="#">
                                    <span>GOOGLE</span>
                                </a>
                           </div>
                        </div>  
                    </div>
                </div>
            </>
        )
    }
}

export default FooterSocial