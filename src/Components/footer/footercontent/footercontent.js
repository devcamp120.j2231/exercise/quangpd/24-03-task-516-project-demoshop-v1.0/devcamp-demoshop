import { Component } from "react";
import style from "../../../App.module.css";
class FooterContent extends Component{
    render(){
        return(
            <>
                <div>
                    <div className="container">
                            <div className="row mt-2">
                                <div className="col-sm-4">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <p>©  2018 . All Rights Reserved.</p>
                                </div>
                                <div className="col-sm-4">
                                    <h5>Contacts</h5>
                                    <dl className="contact-list"><dt>Address:</dt><dd>Kolkata, West Bengal, India</dd></dl>
                                    <dl className="contact-list"><dt>email:</dt><dd><a href="mailto:#" style={{color : "hsla(0,0%,100%,.8)"}}>info@example.com</a></dd></dl>
                                    <dl className="contact-list"><dt>phones:</dt><dd><a href="tel:#" style={{color : "hsla(0,0%,100%,.8)"}}>+91 99999999</a> <span>or</span> <a href="tel:#" style={{color : "hsla(0,0%,100%,.8)"}}>+91 11111111</a></dd></dl>
                                </div>
                                <div className="col-sm-4">
                                    <h5>Links</h5>
                                    <ul className={style.navList}>
                                        <li>
                                            <a href="#about">About</a>
                                        </li>
                                        <li>
                                            <a href="#projects">Projects</a>
                                        </li>
                                        <li>
                                            <a href="#blog">Blog</a>
                                        </li>
                                        <li>
                                            <a href="#contacts">Contacts</a>
                                        </li>
                                        <li>
                                            <a href="#pricing">Pricing</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                    </div>
                </div>
            </>
        )
    }
}

export default FooterContent