import { Component } from "react"
import style from "../../App.module.css";
import FooterContent from "./footercontent/footercontent";
import FooterSocial from "./footersocial/footersocial";
class Footer extends Component{
    render() {
        return(
            <>
                <div className={style.footer}>
                    {/* Footer Content */}
                    <FooterContent/>

                    {/* Footer Social */}
                    <FooterSocial/>
                </div>
            </>
        )
    }
}

export default Footer