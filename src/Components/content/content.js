import { Component } from "react";
import gDataList from "../../data";
import Style from "../../App.module.css";
const gDataObj = JSON.parse(gDataList);

class Content extends Component{
    loadProduct(){

    }

    render(){
        return (
            <>
                <div className="container">
                    <h3 className="center">
                        Product List
                    </h3>
                    <p>
                        Showing 1 - 9 of 24 products
                    </p>
                    <div className="product">
                        <div className="row">
                            {gDataObj.Products.map((value,index)=>{
                                if(index < 9){
                                    return <div className="col-sm-4 mt-3">
                                    <div className="card">
                                        <div className="card-content">
                                            <div className="card-header">
                                                <h5 className="text-center text-primary">{value.Title}</h5>
                                            </div>
                                            <div className="card-body">
                                                <div className="text-center">
                                                    <img width={"200px"} height={"200px"} src={value.ImageUrl}/>
                                                </div>
                                                <div className="card-text mt-2">
                                                    <p className={Style.ellipsis}>
                                                        {value.Description}
                                                    </p>
                                                </div>
                                                <div className="card-text">
                                                    <p><b>Category: </b>{value.Category}</p>
                                                </div>
                                                <div className="card-text">
                                                    <p><b>Made by: </b>{value.Manufacturer}</p>
                                                </div>
                                                <div className="card-text">
                                                    <p><b>Organic: </b>{value.Organic}</p>
                                                </div>
                                                <div className="card-text">
                                                    <p><b>Price: </b>${value.Price}</p>
                                                </div>
                                            </div>
                                            <div className="card-footer text-center">
                                                <button className="btn btn-primary">Add to Cart</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    

                                    
                                }
                            })}
                            

                           
                        </div>
                    </div>
                </div>
            </>
        )
    }
}


export default Content