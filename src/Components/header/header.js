import { Component } from "react";

import Logo from "./headerLogo/headerlogo";
import Navbar from "./navbar/navbar";
class Header extends Component{
    render(){
        return(
            <>
                <Logo/>
                <Navbar/>
            </>
        )
    }
}

export default Header