import { Component } from "react";
import style from "../../../App.module.css";

class Navbar extends Component{
    render() {
        return (
            <>
                <div className={style.navbar}>
                    <a href="#" className={style.a}>
                        <p className={style.navbarContent}>Home</p>
                    </a>
                </div>
                
            </>
        )
    }
}

export default Navbar