import { Component } from "react";
import logo from "../../../assets/images/reactJs.png";
import style from "../../../App.module.css";
class Logo extends Component{
    render() {
        return (
            <>
                <div>
                    <div className="container-fluid">
                        <div className="row mt-2 text-center" >
                            <div className="col-sm-4">
                                <img src={logo} width= "60px" style={{cursor : "pointer"}} />
                            </div>
                            <div className="col-sm-8">
                                <h1 style={{float : "left" ,paddingLeft : "200px"}}>React Store</h1>
                            </div>
                        </div>
                        <div className="row mt-2 text-center">
                            <div className="col-sm-12">
                                <p>Demo App Shop24h v1.0</p>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Logo