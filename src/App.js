//Import thư viện boostrap css
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./Components/header/header";
import Footer from "./Components/footer/footer";
import Content from "./Components/content/content";
function App() {
  return (
    <div className="container-fluid">
      <Header/>
      <Content/>
      <Footer/>
    </div>
  );
}

export default App;
